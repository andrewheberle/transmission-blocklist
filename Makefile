AGENT := Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36
URL := https://www.iblocklist.com/lists.php

.PHONY: clean

public: list.gz
	mkdir -p public
	mv list.gz public

list.gz:
	wget --user-agent "$(AGENT)" -q -O - "$(URL)" | sed -n "s/.*value='\(http:.*=p2p.*\)'.*/\1/p" | xargs -I "{}" wget --user-agent "$(AGENT)" -q -O - "{}" | gunzip | egrep -v '^#' > list
	gzip list

clean:
	rm -rf public
	rm -f list
	rm -f list.gz